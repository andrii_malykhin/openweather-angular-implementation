# Open Weather App implementation using Angular.js and Webpack #

### Short architectural description ###

* During this test I tried to create component-like structure with Angular.js and Webpack.
* I know that this irregular flow for this framework but it was very interesting to try it with popular architectural flow.

### For running app ###

`npm install` then `npm start`
