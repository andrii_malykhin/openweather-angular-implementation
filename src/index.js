import angular from 'angular';
import ngRoute from 'angular-route';
import registerComponents from './components';
import registerServerService from './services/server.service';

const ngModule = angular.module('WeatherApp', [
  ngRoute,
]);

registerComponents(ngModule);
registerServerService(ngModule);

ngModule.constant('ROUTES', (function () {
  return {
    CITY: '/city/:subTab',
    LIST: '/list',
  };
})());

ngModule.config(['$routeProvider', 'ROUTES', function ($routeProvider, ROUTES) {
  const resolveObject = {
    resolveCitiesList: ['serverService', function (serverService) {
      return serverService.getCitiesList();
    }],
    resolveCityWeather: ['serverService', '$route', function (serverService, $route) {
      return serverService.getCityWeather($route.current.params.id);
    }],
    resolveCityForecast: ['serverService', '$route', function (serverService, $route) {
      return serverService.getCityForecast($route.current.params.id);
    }],
  };

  $routeProvider
    .when(ROUTES.CITY, {
      template: '<city city="resolveCityWeather" forecast="resolveCityForecast"></city>',
      resolve: {
        resolveCityForecast: resolveObject.resolveCityForecast,
        resolveCityWeather: resolveObject.resolveCityWeather,
      },
      controller: ($scope, resolveCityForecast, resolveCityWeather) => {
        $scope.resolveCityForecast = resolveCityForecast;
        $scope.resolveCityWeather = resolveCityWeather;
      },
    })
    .when(ROUTES.LIST, {
      template: '<list cities="resolveCitiesList"></list>',
      resolve: { resolveCitiesList: resolveObject.resolveCitiesList },
      controller: ($scope, resolveCitiesList) => {
        $scope.resolveCitiesList = resolveCitiesList;
      },
    })
    .otherwise({
      redirectTo: ROUTES.LIST,
    });
}]);
