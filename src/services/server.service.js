import angular from 'angular';

const urlPreffix = 'https://api.openweathermap.org/data/2.5/';
const urlPostfix = '&units=metric&appid=08686dc84b6d8a519f9e2c6ba3132606';

export default ngModule =>
  ngModule.service('serverService', ['$http', '$q', function ($http, $q) {
    this.getCitiesList = function () {
      return $http.get(`${urlPreffix}group?id=2759794,706483,3067696,2747891,756135${urlPostfix}`)
        .then((response) => {
          if (!angular.isObject(response.data)) {
            return $q.reject(response.data);
          }
          return response.data.list;
        }, response => $q.reject(response.data));
    };

    this.getCityWeather = function (cityId) {
      return $http.get(`${urlPreffix}weather?id=${cityId}${urlPostfix}`)
        .then((response) => {
          if (!angular.isObject(response.data)) {
            return $q.reject(response.data);
          }
          return response.data;
        }, response => $q.reject(response.data));
    };

    this.getCityForecast = function (cityId) {
      return $http.get(`${urlPreffix}forecast/?id=${cityId}${urlPostfix}`)
        .then((response) => {
          if (!angular.isObject(response.data)) {
            return $q.reject(response.data);
          }
          return response.data.list;
        }, response => $q.reject(response.data));
    };
  }]);
