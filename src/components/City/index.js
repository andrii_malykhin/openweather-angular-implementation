import moment from 'moment';

export default (ngModule) => {
  ngModule.directive('city', () => {

    return {
      restrict: 'E',
      replace: true,
      scope: {
        city: '=',
        forecast: '=',
      },
      template: require('./index.html'),
      controller($scope) {
        $scope.Math = window.Math;

        $scope.getDate = (time = new Date()) => moment(time).format('HH:mm MMM D');

        $scope.convertWindDeg = (deg) => {
          const val = Math.floor(deg / 22.5 + 0.5);
          const directionsArr = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];
          return directionsArr[val % 16];
        };
      },
    };
  });
};
