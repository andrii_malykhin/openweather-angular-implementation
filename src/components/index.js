import registerCity from './City';
import registerList from './List';

export default (ngModule) => {
  registerCity(ngModule);
  registerList(ngModule);
};
