import './index.scss';

export default (ngModule) => {
  ngModule.directive('list', () => {

    return {
      restrict: 'E',
      replace: true,
      scope: {
        cities: '=',
      },
      template: require('./index.html'),
      controller($scope) {
        $scope.Math = window.Math;
      },
    };
  });
};
